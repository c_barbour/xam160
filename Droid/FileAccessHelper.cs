﻿using System;
using Xamarin.Forms;
using People.Droid;

[assembly: Dependency(typeof(FileAccessHelper))]

namespace People.Droid
{
	public class FileAccessHelper: IFileAccessHelper
	{
		public FileAccessHelper()
		{
		}

		#region IFileAccessHelper implementation

		public string GetLocalFilePath(string filename)
		{
			string path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
			return System.IO.Path.Combine(path, filename);
		}
		#endregion
	}
}

