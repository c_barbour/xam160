﻿using System;
using Xamarin.Forms;
using People.iOS;

[assembly: Dependency(typeof(FileAccessHelper))]

namespace People.iOS
{
	public class FileAccessHelper: IFileAccessHelper
	{		
		#region IFileAccessHelper implementation

		public string GetLocalFilePath(string filename)
		{
			string docFolder = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
			string libFolder = System.IO.Path.Combine(docFolder, "..", "Library", "Databases");

			if(!System.IO.Directory.Exists(libFolder))
			{
				System.IO.Directory.CreateDirectory(libFolder);
			}

			return System.IO.Path.Combine(libFolder, filename);
		}

		#endregion
	}
}

