﻿using System;
using System.Collections.Generic;
using System.Linq;
using People;
using SQLite;
using System.Threading.Tasks;

namespace People
{
	public class PersonRepository
	{
		private SQLiteAsyncConnection sqlConnection;
		public string StatusMessage { get; set; }

		public PersonRepository(string dbPath)
		{
			// TODO: Initialize a new SQLiteConnection
			sqlConnection = new SQLiteAsyncConnection(dbPath);
			// TODO: Create the Person table
			sqlConnection.CreateTableAsync<Person>();
		}

		public async Task AddNewPersonAsync(string name)
		{
			int result = 0;
			try
			{
				//basic validation to ensure a name was entered
				if (string.IsNullOrEmpty(name))
					throw new Exception("Valid name required");

				// TODO: insert a new person into the Person table
				result = await sqlConnection.InsertAsync(new Person()
				{
					Name = name,
				}).ConfigureAwait(continueOnCapturedContext: false);

				StatusMessage = string.Format("{0} record(s) added [Name: {1}]", result, name);
			}
			catch (Exception ex)
			{
				StatusMessage = string.Format("Failed to add {0}. Error: {1}", name, ex.Message);
			}

		}

		public Task<List<Person>> GetAllPeople()
		{
			// TODO: return a list of people saved to the Person table in the database
			return sqlConnection.Table<Person>().ToListAsync();
		}
	}
}