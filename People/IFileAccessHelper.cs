﻿using System;

namespace People
{
	public interface IFileAccessHelper
	{
		string GetLocalFilePath(string filename);
	}
}

