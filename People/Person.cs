﻿using System;
using SQLite;

namespace People
{
	[Table("people")]
	public class Person
	{
		[PrimaryKey, AutoIncrement] public int Id { get; private set; }
		[MaxLength(250), Unique]public string Name { get; set; }

		public Person()
		{
		}
	}
}

